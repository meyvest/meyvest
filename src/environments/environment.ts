// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBhXz8WhgCO7IqNdF06yNlhH3gJ9jvriHw",
    authDomain: "meyvest-web-app.firebaseapp.com",
    databaseURL: "https://meyvest-web-app.firebaseio.com",
    projectId: "meyvest-web-app",
    storageBucket: "meyvest-web-app.appspot.com",
    messagingSenderId: "801382157346"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
