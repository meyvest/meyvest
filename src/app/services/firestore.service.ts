import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import {  Observable, of } from 'rxjs';
import { map, finalize, switchMap } from 'rxjs/operators';

import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  firestoreCollection: AngularFirestoreCollection<any>;
  firestoreDocument: AngularFirestoreDocument<any>;
  user$: Observable<User>;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth, private router: Router) { 
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
          // Logged in
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          // Logged out
          return of(null);
        }
      })
    )
  }

  getSubscribers(): Observable<any[]> {
    this.firestoreCollection = this.afs.collection('subscribers', ref => {
      return ref.orderBy('time_created').limit(40);;
    })
    return this.firestoreCollection.valueChanges();
  }

  getSubscribersWithIDs(): Observable<any[]> {
    this.firestoreCollection = this.afs.collection('subscribers', ref => {
      return ref.orderBy('time_created', 'desc').limit(40);
    })
    return this.firestoreCollection.snapshotChanges().pipe(map(actions => {
      return actions.map (a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  addSubscriber(data: any) {
    this.firestoreCollection = this.afs.collection('subscribers');
    return this.firestoreCollection.add(data);
  }

  updateSubscriber(data: any, id: string) {
    this.firestoreDocument = this.afs.doc('subscribers/' + id);
    return this.firestoreDocument.set(data, { merge: true });
  }

  deleteSubscriber(id: string) {
    this.firestoreDocument = this.afs.doc('subscribers/' + id);
    return this.firestoreDocument.delete();
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.router.navigate(['/']);
  }

  private updateUserData(user) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data = { 
      uid: user.uid,
      email: user.email, 
      displayName: user.displayName, 
      photoURL: user.photoURL
    }

    return userRef.set(data, { merge: true })
  }
}
