import { TestBed } from '@angular/core/testing';

import { MobileDesktopIdentifierService } from './mobile-desktop-identifier.service';

describe('MobileDesktopIdentifierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MobileDesktopIdentifierService = TestBed.get(MobileDesktopIdentifierService);
    expect(service).toBeTruthy();
  });
});
