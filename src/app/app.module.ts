import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { RoutingComponentComponent } from "./routing-component/routing-component.component";

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { AdminContactComponent } from './admin-contact/admin-contact.component';
import { ContactPipe } from './pipes/contact.pipe';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  {path: '', component: LandingComponent},
  {
    path: 'admin',
    children: [
      {path: 'contact', component: AdminContactComponent}
    ]
  },
  {path: 'privacy-policy', component: PrivacyPolicyComponent}
]

export const firebaseConfig = environment.firebase;

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    AdminContactComponent,
    RoutingComponentComponent,
    ContactPipe,
    PrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
