import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contact'
})
export class ContactPipe implements PipeTransform {

  transform(items: any[], key: string, search: string): any[] {
    if (!items) {
      return null;
    } 

    // Check for sorting key, and search values
    if (key) items = this.sortItems(items, key);
    if(search) items = this.searchValue(items, search);

    return items;
  }

  sortItems(items: any[], key: string) {
    switch (key) {
      case 'unread':
        items.sort((a, b) => a.read == b.read ? 0 : a.read ? 1 : -1);
        break;
      case 'read':
        items.sort((a, b) => a.read == b.read ? 0 : a.read ? -1 : 1);
        break;
      case 'no-quote':
        items.sort((a, b) => a.quote == b.quote ? 0 : a.quote ? 1 : -1);
        break;
      case 'quote':
        items.sort((a, b) => a.quote == b.quote ? 0 : a.quote ? -1 : 1);
        break;
      case 'time-desc':
        items.sort((a, b) => a.time_created > b.time_created ? -1 : a.time_created < b.time_created ? 1 : 0)
        break;
      case 'time-asc':
        items.sort((a, b) => a.time_created < b.time_created ? -1 : a.time_created > b.time_created ? 1 : 0)
        break;
    }
    return items;
  }

  searchValue(items: any[], search: string) {
    var searchFunction = function(element, search) {
      var flag: boolean = false;
      if (element.email.toLowerCase().includes(search.toLowerCase())) {
        flag = true;
      } 
      return flag;
    }

    items = items.filter(singleItem => searchFunction(singleItem, search))
    return items;
  }

}
