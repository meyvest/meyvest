import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from "../services/firestore.service";
import { MobileDesktopIdentifierService } from "../services/mobile-desktop-identifier.service";
declare var $: any;

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  form: FormGroup;
  isMobile: boolean = false;
  isQuote: boolean = false;

  constructor(private formBuilder: FormBuilder, private firestoreService: FirestoreService, private mobileIndentifier: MobileDesktopIdentifierService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      name: [null, Validators.required],
      time_created: [new Date()]
    })
    this.isMobile = this.mobileIndentifier.isMobile();
  }

  // Uncomment if the hello block comes back
  // ngAfterViewInit(): void {
  //   this.fixHelloText();
  // }

  // fixHelloText() {
  //   if (this.isMobile) {
  //     const style = 'top: ' + this.getRedContainerMobileHeight() + 'px;';
  //     $("#hello-mobile").attr("style", (style))
  //   } else {
  //     const style = 'top: ' + this.getRedContainerHeight() + 'px;';
  //     $("#hello").attr("style", (style))
  //   }
  // }

  // getRedContainerHeight() {
  //   return $("#container-info").offset().top - 85;
  // }

  // getRedContainerMobileHeight() {
  //         return ($("#container-info-mobile").offset().top - 42);
  // }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileIndentifier.isMobile();
    // this.fixHelloText();
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    var submitData = this.form.value;
    submitData['read'] = false;
    submitData['quote'] = this.isQuote;
    
    this.firestoreService.addSubscriber(submitData).then(() => {
        $('#success-modal').modal('show');
    })
  }

  toggleQuote() {
    this.isQuote = !this.isQuote;
  }
}
