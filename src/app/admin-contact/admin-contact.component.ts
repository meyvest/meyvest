import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { FirestoreService } from "../services/firestore.service";
declare var $: any;

@Component({
  selector: 'app-admin-contact',
  templateUrl: './admin-contact.component.html',
  styleUrls: ['./admin-contact.component.css']
})
export class AdminContactComponent implements OnInit {
  contacts: Observable<any[]>;
  contactsList: any[];
  contact: any;
  searchValue = "";
  sortKey = "";
  prevSortKeySelected = "";

  constructor(public firestoreService: FirestoreService) {}

  ngOnInit() {
    this.contacts = this.firestoreService.getSubscribersWithIDs();
    this.contacts.subscribe(data => {
      this.contactsList = data;
    })
    $("#showContactModal").on("hidden.bs.modal", function(e) {
      $("#btn-delete-confirm").attr("style", "display: none !important");
      $("#btn-delete").attr("style", "display: block !important");
    });
  }

  getEmailStr() {
    return "mailto:" + this.contact.email;
  }

  updateContact(i) {
    this.contact = this.contactsList[i];
    const updateData = { read: true };
    this.firestoreService.updateSubscriber(updateData, this.contact.id);
  }

  setUnreadFlag() {
    const updateData = { read: false };
    this.firestoreService.updateSubscriber(updateData, this.contact.id);
  }

  switchFilterKey(id: string) {
    this.sortKey = id;
    this.triggerActiveDropdown(id);
  }

  triggerActiveDropdown(id: string) {
    // console.log(id);
    if (!this.prevSortKeySelected) this.prevSortKeySelected = id;

    $("#" + this.prevSortKeySelected)
      .removeClass("bg-primary")
      .removeClass("text-white");
    this.prevSortKeySelected = id;
    $("#" + id)
      .addClass("bg-primary")
      .addClass("text-white");
  }

  onKey(event: any) {
    // without type info
    this.searchValue = event.target.value;
  }

  setDeleteFlag() {
    $("#btn-delete").attr("style", "display: none !important");
    $("#btn-delete-confirm").attr("style", "display: block !important");
  }

  deleteContact() {
    this.firestoreService.deleteSubscriber(this.contact.id);
  }

  getIconBoolean(flag: boolean) {
    if (flag) {
      return 'check_circle_outline';
    } else {
      return 'highlight_off';
    }
  }
}
