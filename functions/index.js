const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const SENDGRID_API_KEY = functions.config().sendgrid.key;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);

exports.firestoreEmail = functions.firestore.document('subscribers/{subscriberId}').onCreate(event => {
    const msg = {
        to: ['mauricior@meyvest.com', 'boycep@meyvest.com'],
        from: 'info@meyvest.com',
        templateId: 'd-4d06eb275d5d450e8a0ddb1a83cb4bc6',
    };
        
    return sgMail.send(msg);
});